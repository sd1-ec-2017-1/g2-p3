# Projeto 3 do Grupo 2 da Disciplina de Sistemas Distribuídos 1 do Curso de Eng. de Computação da UFG

## Documentação

A documentação completa do projeto encontra-se na [wiki](https://gitlab.com/sd1-ec-2017-1/g2-p3/wikis/home)


# Alunos:

+ Matheus
+ Rodrigo
+ Dyonnatan
+ Tiago Henrique
+ Gabriel Rios