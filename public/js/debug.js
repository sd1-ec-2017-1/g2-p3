var chns = ['chn1', 'chn2', 'chn3','teste1'];

function initChannels()
{
	var brd = $('#brd-container');
	var nav = $('#nav-container');

	for(var i = 0; i < chns.length; i++)
	{
		nav.append(
			'<div class="nav-btn" id="div-btn-chn-' + chns[i] + '">' +
			'<button class="" onclick="toggleEl(this.innerHTML)" id="btn-chn-' + chns[i] + '">#' + chns[i] + '<span onclick="removeEl(\'' + chns[i] + '\', \'chn\');"><i class="fa fa-times" aria-hidden="true"></i></span></button>' +
			'</div>');

		brd.append(
			'<div class="brd-unit" id="brd-chn-' + chns[i] + '">' +
			'<div class="chn-left">' +
			'<div class="msg-brd">' +
			'<ul id="msg-chn-' + chns[i] + '"></ul>' +
			'</div>' +
			'<div class="div-msg-input">' +
			'<input type="text" placeholder="Type a message..." class="input-msg" id="ipt-chn-' + chns[i] + '">' +
			'<button type="submit" value="send" onclick="submete_mensagem(\'ipt-chn-' + chns[i] + '\')" class="input-btn" id="snd-chn-' + chns[i] + '"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>' +
			'</div>' +
			'<p></p>'+
			'<div>'+
			'<input type="text" placeholder="Join: Digite o nome do canal" class="input-msg" id="input-join">'+
			'<button type="submit" value="send" onclick="join()" class="input-btn" id="join-btn"></button>'+
			'</div>'+
			'</div>' +
			'<div class="chn-usr">' +
			'<ul id="chn-usr-' + chns[i] + '"></ul>' +
			'</div>' +
			'</div>');

      incluirCanal(chns[i]);
	}
}

function initActive()
{
	$('#btn-chn-home').addClass("btn-active");

  $('#brd-chn-home').addClass("brd-active");

	$('#ipt-chn-home').focus();
}
