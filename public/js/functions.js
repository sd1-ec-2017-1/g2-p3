/*
  Adiciona <mensagem> no <elemento_id>.
*/
function adiciona_mensagem(msg,canal,timestamp) {
	console.log("Canal: "+canal);

	if(typeof canal != "undefined")
	{
		var elCanal = '#msg-chn-' + canal.substr(1);

		$(elCanal).append('<li>' + msg + '</li>');
	}

}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function iniciar() {

	var msg = 'Bem vindo(a), ' + Cookies.get("nick") + '!';
	var msg2 = 'Voce esta conectado(a) a irc://' + Cookies.get("servidor") + '/home';

	$('#msg-chn-home').append('<li>' + msg + '</li>');
	$('#msg-chn-home').append('<li>' + msg2 + '</li>');

	var canal_init = Cookies.get("canal");
	if(canal_init.charAt(0) === '#') canal_init = canal_init.substr(1);

	addEl(canal_init,'chn');
	incluirCanal(canal_init);

	carrega_mensagens(0);
}

function join() {
	var canal = $('.brd-active').find('#input-join').val();

	if(canal.charAt(0) === '#') canal = canal.substr(1);

	addEl(canal,'chn');
	incluirCanal(canal);
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(timestamp) {

	var mensagem = "";
	var horario = "";

	$.get("obter_mensagem/" + timestamp, function (data, status) {
			if (status == "success") {
				var linhas = data;
				for (var i = linhas.length - 1; i >= 0; i--) {
					horario = timestamp_to_date(linhas[i].timestamp);
					mensagem = "[" + horario + "] - " + linhas[i].nick + ": " + linhas[i].msg;

					novo_timestamp = linhas[i].timestamp;

					console.log('timestamp:' + novo_timestamp);

					adiciona_mensagem(mensagem, linhas[i].canal, novo_timestamp);
				}
			}
			else {
				alert("erro: " + status);
			}
		}
	);
	t = setTimeout(
		function () {
			carrega_mensagens(novo_timestamp)
		},
		1000);
}

function submete_mensagem(elem_id_mensagem) {

	var tipo;

	if(elem_id_mensagem.slice(4,7) == 'chn')
	{
		tipo = 'canal';

	}
	else
	{
		tipo = 'pvt';
	}

	var destino = elem_id_mensagem.slice(8);
	destino = "#"+destino;

	var mensagem= document.getElementById(elem_id_mensagem).value;
	document.getElementById(elem_id_mensagem).value = "";

	var msg = '{"timestamp":'+Date.now()+','+
		  '"nick":"'+Cookies.get("nick")+'",'+
			'"'+tipo+'":"'+destino+'",'+
                  '"msg":"'+mensagem+'"}';
	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success:
		function(data,status) {
			if (status == "success") {
			    // nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
}

function trocarMode(elemento){

	var usuario = Cookies.get("nick");
	var args = $("#"+elemento).val();
	var comando = "mode/"+usuario+"/"+args;
$.get(comando, function(data,status) {
		if ( status == "success" ) {

		alert(comando);
}
		});
}

function addEl(desc, type)
{
	if(type == 'chn')
	{
		$('#nav-container').append(
			'<div class="nav-btn" id="div-btn-chn-' + desc + '">' +
			'<button class="" onclick="toggleEl(this.innerHTML)" id="btn-chn-' + desc + '">#' + desc + '<span onclick="removeEl(\'' + desc + '\', \'chn\')"><i class="fa fa-times" aria-hidden="true"></i></span></button>' +
			'</div>');

		$('#brd-container').append(
			'<div class="brd-unit" id="brd-chn-' + desc + '">' +
			'<div class="chn-left">' +
			'<div class="msg-brd">' +
			'<ul id="msg-chn-' + desc + '"></ul>' +
			'</div>' +
			'<div class="div-msg-input">' +
			'<input type="text" placeholder="Type a message..." class="input-msg" id="ipt-chn-' + desc + '">' +
			'<button type="submit" value="send" onclick="submete_mensagem(\'ipt-chn-' + desc + '\')" class="input-btn" id="snd-chn-' + desc + '"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>' +
			'<p></p>'+
			'<div>'+
			'<input type="text" placeholder="Join: Digite o nome do canal" class="input-msg" id="input-join">'+
			'<button type="submit" value="send" onclick="join()" class="input-btn" id="join-btn"></button>'+
			'</div>'+
			'</div>' +
			'</div>' +
			'<div class="chn-usr">' +
			'<ul id="chn-usr-' + desc + '"></ul>' +
			'</div>' +
			'</div>');
	}

	if(type == 'pvt')
	{
		$('#nav-container').append(
			'<div class="nav-btn" id="div-btn-pvt-' + desc + '">' +
			'<button class="" onclick="toggleEl(this.innerHTML)" id="btn-pvt-' + desc + '">' + desc + '<span onclick="removeEl(\'' + desc + '\', \'pvt\')"><i class="fa fa-times" aria-hidden="true"></i></span></button>' +
			'</div>');

		$('#brd-container').append(
			'<div class="brd-unit" id="brd-pvt-' + desc + '">' +
			'<div class="msg-brd">' +
			'<ul id="msg-pvt-' + desc + '"></ul>' +
			'</div>' +
			'<div class="div-msg-input">' +
			'<input type="text" placeholder="Type a message..." class="input-msg" id="ipt-pvt-' + desc + '">' +
			'<button type="submit" value="send" onclick="" class="input-btn" id="snd-pvt-' + desc + '"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>' +
			'</div>' +
			'</div>');
	}
}

function removeEl(desc, type)
{
	$('#div-btn-' + type + '-' + desc).remove();

	$('#brd-' + type + '-' + desc).remove();
}

function toggleEl(el)
{
	var btn_id, brd_id, ipt_id;

	el = el.substring(0, el.indexOf('<'));

	if(el.indexOf('#') > -1)
	{
		btn_id = '#btn-chn-' + el.substring(1);
		brd_id = '#brd-chn-' + el.substring(1);
		ipt_id = '#ipt-chn-' + el.substring(1);
	}
	else
	{
		btn_id = '#btn-pvt-' + el;
		brd_id = '#brd-pvt-' + el;
		ipt_id = '#ipt-pvt-' + el;
	}

	$('.btn-active').removeClass("btn-active");
	$(btn_id).addClass("btn-active");

	$('.brd-active').removeClass("brd-active");
  $(brd_id).addClass("brd-active");

	$(ipt_id).focus();
}

function incluirCanal(canal)
{
	var msg = '{"canal":"' + canal +  '"}';

	$.ajax({
		type: "post",
		url: "/incluir_canal",
		data: msg,
		success:
		function(data,status) {
			if (status == "success") {
					// nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
}

function retirarCanal(canal)
{
	var msg = '{"canal":"' + canal +  '"}';

	$.ajax({
		type: "post",
		url: "/incluir_canal",
		data: msg,
		success:
		function(data,status) {
			if (status == "success") {
					// nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
}
